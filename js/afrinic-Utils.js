class AfrinicUtils{
    formatBytes(a,b){if(0==a)return"0 Bytes";var c=1024,d=b||2,e=["Bytes","KB","MB","GB","TB","PB","EB","ZB","YB"],f=Math.floor(Math.log(a)/Math.log(c));return parseFloat((a/Math.pow(c,f)).toFixed(d))+" "+e[f]}
    objectSize(obj){
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) {
                size++;
            }
        }
        return size;
    }
    getEventDetailsFromCSV(callback){
        $.ajax({
            url: '/CSV',
            method: 'get',
            success: (data) =>{
                callback(data);
            }
        });
    }
    groupData(CSVData, key){
        var grouped = CSVData.reduce((groups, item, index) => {
            var val = item[key];

            if(index <= 1){
                var tempGroups = groups;
                groups = {};
                groups[tempGroups[key]] = groups[tempGroups[key]] || [];
                groups[tempGroups[key]].push(tempGroups);
            }
            
            groups[val] = groups[val] || [];
            groups[val].push(item);
            return groups;
        });

        return grouped;
    }
    addConstantEvents(array, CSVData, key, callback){
        var grouped = this.groupData(CSVData, key);
        callback(grouped);
    }
    getAllUrlParams(url) {
        var queryString = url ? url.split('?')[1] : window.location.search.slice(1);
        var obj = {};
    
        if (queryString) {
            queryString = queryString.split('#')[0];
            var arr = queryString.split('&');
    
            for (var i = 0; i < arr.length; i++) {
                var a = arr[i].split('=');
                var paramName = a[0];
                var paramValue = typeof a[1] === 'undefined' ? true : a[1];
    
                paramName = paramName.toLowerCase();
                if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();
    
                if (paramName.match(/\[(\d+)?\]$/)) {
                    var key = paramName.replace(/\[(\d+)?\]/, '');
                    if (!obj[key]) obj[key] = [];
    
                    if (paramName.match(/\[\d+\]$/)) {
                        var index = /\[(\d+)\]/.exec(paramName)[1];
                        obj[key][index] = paramValue;
                    } else {
                        obj[key].push(paramValue);
                    }
                } else {
                    if (!obj[paramName]) {
                        obj[paramName] = paramValue;
                    } else if (obj[paramName] && typeof obj[paramName] === 'string') {
                        obj[paramName] = [obj[paramName]];
                        obj[paramName].push(paramValue);
                    } else {
                        obj[paramName].push(paramValue);
                    }
                }
            }
        }
    
        return obj;
    }
}

export default AfrinicUtils;