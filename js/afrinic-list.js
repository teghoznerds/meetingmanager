import AfrinicBase from "./afrinic-base.js";

class MeetingManagerList extends AfrinicBase{
    constructor() {
        super();
    }
    init(){
        super.init();
        console.log("Welcome to Meeting Manager List View");
        this.listViewGenerator();
    }
    setElements(){
        this.listViewObject = $(".list-view-container");
        this.dayTemplate = $($("#day-template")[0].import).find("#day-template");
        this.sessionTemplate = $($("#session-template")[0].import).find("#session-template");
        this.eventTemplate = $($("#event-template")[0].import).find("#event-template");
        this.eventItemTemplate = $($("#event-item-template")[0].import).find("#event-item-template");
    }
    bindUIActions(){
    }
    listViewGenerator(){
        var listEvents = "";
        
        this.afrinicUtils.getEventDetailsFromCSV((data) => {
            this.eventConstantHandler(data, data);
            var groupedData = this.afrinicUtils.groupData(data, "Day");

            for(var i = 0; i < Object.keys(groupedData).length; i++){
                var key = Object.keys(groupedData)[i];
                //var event = groupedData[key][0];
                this.listViewObject.append(this.dayCreator(groupedData[key]));
            }
        });
    }
    dayCreator(data){
        var order = ["Morning", "Lunch", "Afternoon", "Networking"];
        var dayUI = $(this.dayTemplate.html());

        var groupedData = this.afrinicUtils.groupData(data, "Session");

        order.forEach((session) => {
            var key = session;           
            var event = groupedData[key][0];

            if(event !== undefined){
                dayUI.children(".day-display").html(`Day ${event.Day}`);
                dayUI.children(".day-body").children(".session-row").append(this.sessionCreator(groupedData[key]));
            }      
        });

        return dayUI;
    }
    sessionCreator(data){       
        var sessionUI = $(this.sessionTemplate.html());

        for(var i = 0; i < Object.keys(data).length; i++){
            var key = Object.keys(data)[i];
            var event = data[key];
            sessionUI.children(".card").children(".card-header").children(".card-title").html(`${event.Session} Session`);
            sessionUI.children(".card").children(".card-body").append(this.eventCreator(event));
        }
        return sessionUI;
    }   
    eventCreator(data){
        var eventUI = $(this.eventTemplate.html());    
        eventUI.append(this.eventItemCreator(data));
        return eventUI;
    }
    eventItemCreator(data){
        var dayEventItem = $(this.eventItemTemplate.html());
        dayEventItem.children(".headingsection").children(".title").html(`${data.Title}`);
        var badgeDuration = `<span class="badge badge-primary">${data.Duration} mins</span>`;
        dayEventItem.children(".headingsection").children(".duration").html(`${data.Start} ${badgeDuration}`);
        dayEventItem.children(".description").html(`${data.Description}`);
        var speakerBadge = "";
        if(data.Speaker !== ""){
            speakerBadge = `<span class="badge badge-success badge-md"><i class="fa fa-user"></i>&nbsp;${data.Speaker}</span>`;
        }
        
        dayEventItem.children(".speaker").html(`${speakerBadge}`);
        return dayEventItem;
    }
    eventConstantHandler(array, CSVData){
        this.afrinicUtils.addConstantEvents(array, CSVData, "Day", (groupedData) => {
            for(var i = 0; i < Object.keys(groupedData).length; i++){
                var key = Object.keys(groupedData)[i];
                var event = groupedData[key][0];
    
                var lunchTimeLine = {
                    "Title": "Lunch Time",
                    "Date": `${event.Date}`,
                    "Footer": "",
                    "Description": '<div class="text-center align-center"><img class="img-responsive lunch" src="./images/lunchtime.png" width="120" height="120" alt="Lunch Time" /></div>',
                    "Day": key,
                    "Session": "Lunch",
                    "Start": "12:30",
                    "Duration": "90",
                    "Speaker": ""
                }
    
                array.push(lunchTimeLine);
            }
        })
    }
    dateComparison(a, b){
        //console.log("date: ", new Date(`${b.Date}T${b.Start}`));
        return new Date(`${b.Date}T${b.Start}:00`) - new Date(`${a.Date}T${a.Start}:00`);
    }
}


var meetingManagerList = new MeetingManagerList();
meetingManagerList.init();