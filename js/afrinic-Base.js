import AfrinicUI from "./afrinic-UI.js";
import AfrinicUtils from "./afrinic-Utils.js";

class AfrinicBase {
    constructor(options) {
        this.afrinicUI = new AfrinicUI;
        this.afrinicUtils = new AfrinicUtils;
        this.username = $(".username");
    }
    init() {
        this.setElements();
        this.setProperties();
        this.bindUIActions();
        this.afrinicUI.showUsername(this.username);
    }
    setElements() {
        
    }
    setProperties() {

    }
    bindUIActions() {

    }
}

export default AfrinicBase;