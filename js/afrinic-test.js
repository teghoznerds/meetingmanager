import AfrinicBase from "../js/afrinic-base.js";

class MeetingManagerTest extends AfrinicBase{
    constructor() {
        super();
    }
    init(){
        super.init();
        console.log("Welcome to Meeting Manager Test");
        this.test();
    }
    test(){
        QUnit.test("Afrinic Utils", (assert) => {
            assert.ok(this.afrinicUtils !== undefined, "Passed!" );
        });

        QUnit.test("Afrinic UI", (assert) => {
            assert.ok(this.afrinicUI !== undefined, "Passed!" );
        });

        QUnit.test("username", (assert) => {
            assert.ok(this.username.get(0) == $(".username").get(0), "Passed!" );
        });

        QUnit.test("Afrinic Utils formatBytes", (assert) => {
            assert.ok(this.afrinicUtils.formatBytes(1024) == "1 KB", "Passed!" );
        });

        QUnit.test("Afrinic Utils getAllUrlParams", (assert) => {
            var url = "http://localhost:3000/Test?msg=Afrinic";
            assert.ok(this.afrinicUtils.getAllUrlParams(url).msg == "afrinic", "Passed!" );
        });

        QUnit.test("Afrinic Utils groupData", (assert) => {
            var sampleData = [{ key: "1", value: "One"}, { key: "1", value: "Two"}, { key: "2", value: "Three"}];
            var groupedData = this.afrinicUtils.groupData(sampleData, "key");
            assert.ok(Object.keys(groupedData).length == 2, "Passed!" );
            assert.ok(groupedData["1"].length == 2, "Passed!" );
        });
    }
}


var meetingManagerTest = new MeetingManagerTest();
meetingManagerTest.init();