class AfrinicUI{
    showUsername(element){
        $.ajax({
            url: '/GetUsername',
            method: 'get',
            success: (data) =>{
                element.html(data);
            }
        });
    }
    showMessage(alert, element, heading, main, supporting, type){       
        alert.children(".main-alert").addClass(type);
        alert.children(".main-alert").children(".alert-heading").html(heading);
        alert.children(".main-alert").children(".main-message").html(main);
        alert.children(".main-alert").children(".supporting-message").html(supporting);
        element.append(alert);
    }
    showHideSection(element, action, options){
        if(action === "show"){
            element.show(options);
        }
        else if(action === "hide"){
            element.hide(options);
        }
        else{

        }
    }
}


export default AfrinicUI;