import AfrinicBase from "./afrinic-base.js";

class MeetingManagerTimeline extends AfrinicBase{
    constructor() {
        super();
    }
    init(){
        super.init();
        console.log("Welcome to Meeting Manager Timeline View");
        this.initializeTimeline();
    }
    setElements(){
        this.timeLineObject = $("#myTimeline");
    }
    bindUIActions(){
    }
    initializeTimeline(){
        this.afrinicUtils.getEventDetailsFromCSV((details) => {
            this.timeLineObject.albeTimeline(this.formatData(details));
        });
    }
    formatData(data){
        var finalData = [];
        data.map((item) => {
            //console.log("item: ", item);
            var timeLineitem = {
                day: item.Day,
                time: `${item.Date} ${item.Start}`,
		        header: item.Speaker,
		        body: [{
                    tag: 'h3',
                    content: item.Title
                },
                {
                    tag: 'p',
                    content: item.Description
                }],
		        footer: `<span class="">${item.Session} Session </span> <span class="pull-right"><span class="badge badge-primary">${item.Duration} mins</span></span>`
            };

            finalData.push(timeLineitem);           
        });

        this.eventConstantHandler(finalData, data);
        return finalData;
    }
    eventConstantHandler(array, CSVData){
        this.afrinicUtils.addConstantEvents(array, CSVData, "Day", (groupedData) => {
            for(var i = 0; i < Object.keys(groupedData).length; i++){
                var key = Object.keys(groupedData)[i];
                var event = groupedData[key][0];
    
                var lunchTimeLine = {
                    day: event.Day,
                    time: `${event.Date} 12:30`,
                    header: 'Lunch',
                    body: [{
                        tag: 'h1',
                        content: 'Lunch'
                    },
                    {
                        tag: 'div',
                        attr: {
                            cssclass: 'text-center align-center'
                        },
                        content: '<img class="img-responsive lunch" src="./images/lunchtime.png" width="120" height="120" alt="Lunch Time" />'
                    },
                    {
                        tag: 'p',
                        attr: {
                            cssclass: 'text-center align-center'
                        },
                        content: 'Drinks in the Lobby'
                    }],
                    footer: null
                };
    
                array.push(lunchTimeLine);
            }
        })
    }
}


var meetingManagerTimeline = new MeetingManagerTimeline();
meetingManagerTimeline.init();