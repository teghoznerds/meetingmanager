import AfrinicBase from "./afrinic-base.js";

class MeetingManager extends AfrinicBase{
    constructor() {
        super();
    }
    init(){
        super.init();
        console.log("Welcome to Meeting Manager");
    }
    setElements(){
        this.meetingSection = $(".meeting-section");
        this.noCSVSection = $(".no-csv");
        this.csvName = $(".csv-name");
        this.csvSize = $(".csv-size");
        this.csvDateCreated = $(".csv-date-created");
        this.btnFileDelete = $(".remove-csv");
        this.csvPreviewSection = $(".csv-preview-section");
        this.messageHolder = $(".message-holder");
        this.alertTemplate = $($("#alert")[0].import).find("#alert");
    }
    bindUIActions(){
        //decide whether to display meeting section if CSV is not available
        this.handleMeetingDisplay();
        //delete uploaded CSV file
        this.handleFileDelete(this.btnFileDelete, (res) => {
            //decide whether to display meeting section if CSV is not available
            this.handleMeetingDisplay();
            var alert = $(this.alertTemplate.html());       
            this.afrinicUI.showMessage(alert, this.messageHolder, "Info", "File Deleted Successfully!!", "", "alert-success");
        });

        this.handleFeedbackDisplay(this.messageHolder);
    }
    handleCSVInfo(){
        this.afrinicUtils.getEventDetailsFromCSV((result) => {
            this.csvPreviewSection.html(JSON.stringify(result));
        });
    }
    checkIfCSVIsAvailable(callback){
        $.ajax({
            url: '/CheckIfCSVExist',
            method: 'get',
            success: (data) =>{
                callback(data)
            }
        });
    }
    handleMeetingDisplay(){
        this.checkIfCSVIsAvailable((available) =>{
            if(available){
                this.afrinicUI.showHideSection(this.meetingSection, "show");
                this.afrinicUI.showHideSection(this.noCSVSection, "hide");
                this.showCSVDetails();
                this.handleCSVInfo();
            }
            else{
                this.afrinicUI.showHideSection(this.noCSVSection, "show");
                this.afrinicUI.showHideSection(this.meetingSection, "hide");
            }
        })
    }
    handleFileDelete(button, callback){
        button.click((element) => {
            $.ajax({
                url: '/DeleteCSVFile',
                method: 'get',
                success: (data) =>{
                    callback(data);
                }
            });
        });
    }
    handleFeedbackDisplay(element){       
        var params = this.afrinicUtils.getAllUrlParams(self.location.href);

        if(params.msg !== undefined){ 
            var suuportingMessage = `Please see <a href='/FormatRules'> CSV Format </a>.`;   
            var alert = $(this.alertTemplate.html());       
            this.afrinicUI.showMessage(alert, element, "<i class='fa fa-info-circle'></i>&nbsp;Info", decodeURIComponent(params.msg), suuportingMessage, `alert-${params.status}`);
        }
    }
    showCSVDetails(){
        $.ajax({
            url: '/GetCSVFileDetails',
            method: 'get',
            success: (data) =>{
                this.csvName.html(data.fileName);
                this.csvSize.html(this.afrinicUtils.formatBytes(data.size));
                this.csvDateCreated.html(data.birthtime);
            }
        });
    }  
}


var meetingManager = new MeetingManager();
meetingManager.init();