const assert = require('assert');
const csv = require('csvtojson');
var csvalidator = require('../csv-validator');

var testPath = './test/test-csv.csv';
var validator = new csvalidator(testPath, csv);


describe('validator', function() {
    it('isValid', function(done) {
        validator.isValid((result) => {
            assert.equal(false, result.status);
            done();
        });
    });
});