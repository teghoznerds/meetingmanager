class CSValidator{
    constructor(path, csv){
        this._path = path; 
        this._csv = csv;
    }

    validator(regex, field, callback){  
        var obj = { status: true, message : "CSV Validated and Uploaded"};

        this._csv().fromFile(this._path).then((jsonObj)=>{
            var result =  jsonObj.filter(row => {
                return regex.test(row[field]) == false;
            });

            if(result.length > 0){
                obj = {
                    status: false,
                    message: "Uploaded CSV Title Field Contains a Number. Please fix and upload again"
                };
            }

            callback(obj);
        });       
    }

    isValid(regex, field, callback){
        this.validator(regex, field, (result) => {
            callback(result);
        });
    }
}

module.exports = CSValidator