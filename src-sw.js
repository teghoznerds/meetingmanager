﻿importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');

if (workbox) {
    console.log(`Yay! Workbox is loaded 🎉`);
} else {
    console.log(`Boo! Workbox didn't load 😬`);
}

//Force Developent/Production Builds
workbox.setConfig({ debug: true });

//Initialize google analytics
workbox.googleAnalytics.initialize();

// Enable navigation preload.
workbox.navigationPreload.enable();

//take control immediately
workbox.core.skipWaiting();
workbox.core.clientsClaim();

const CACHE_VERSION = "1.0.0.4";
const CACHE_NAME = "Afrinic-" + "-" + CACHE_VERSION;

//This is our custom handler to recieve pushed notifications
self.addEventListener('push', event => {
    //console.log("event: ", event);
    const options = {
        body: event.data.text(),
        icon: params.notificationicon,
        vibrate: [100, 50, 100],
        data: {
            dateOfArrival: Date.now(),
            primaryKey: 1
        },
        actions: [
            {
                action: '/', title: 'Go to '+params.siteshortname,
                icon: '/content/yes.png'
            },
            {
                action: 'close', title: 'Close the notification',
                icon: '/content/no.png'
            },
        ]
    };

    event.waitUntil(
        self.registration.showNotification('Push Notification', options)
    );
});

const propertyHomeHandler = new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'afrinic-pages',
    plugins: [
      new workbox.expiration.Plugin({
          maxEntries: 50,
      })
    ]
});

const offlineHandler = new workbox.strategies.CacheFirst({
    cacheName: 'offline-page',
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 50,
        })
    ]
});

workbox.core.setCacheNameDetails({
	prefix: CACHE_NAME + '',
	suffix: CACHE_NAME + '',
	precache: CACHE_NAME + '-precache',
	runtime: CACHE_NAME+ '-runtime'
});

workbox.routing.registerRoute('/', args => {
    return propertyHomeHandler.handle(args).then(response => {
        if (!response) {
            return caches.match('/Offline/');
        } else if (response.status === 404) {
            return caches.match('/Offline/');
        }
        return response;
    });
});

workbox.routing.registerRoute('/Home/Index/', args => {
    return propertyHomeHandler.handle(args).then(response => {
        if (!response) {
            return caches.match('/Offline/');
} else if (response.status === 404) {
            return caches.match('/Offline/');
}
        return response;
});
});

workbox.routing.registerRoute('/Offline/', args => {
    return offlineHandler.handle(args).then(response => {
        if (!response) {
            return caches.match('/Offline.html');
} else if (response.status === 404) {
            return caches.match('/Offline.html');
}
        return response;
});
});

workbox.routing.registerRoute('/fonts/*',
    new workbox.strategies.CacheOnly()
);

workbox.routing.registerRoute('/vendor/*',
    new workbox.strategies.CacheOnly()
);


workbox.routing.registerRoute('/css/*',
    new workbox.strategies.CacheOnly()
);

workbox.routing.registerRoute('/js/*',
    new workbox.strategies.CacheOnly()
);

//handle all CDN related request
workbox.routing.registerRoute(
    /\.(?:js|css)$/,
    new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'static-resources',
})
);

workbox.routing.registerRoute(
  '/\.(?:png|gif|jpg|jpeg|svg)$/',
  new workbox.strategies.CacheFirst({
    cacheName: 'images',
    plugins: [
        new workbox.expiration.Plugin({
    maxEntries: 60,
    maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
}),
],
}),
);

workbox.routing.registerRoute(
    /.*(?:bootstrapcdn)\.com/,
    new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'googleapis',
})
);


workbox.routing.registerRoute(
    /.*(?:aspnetcdn)\.com/,
    new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'googleapis',
})
);

workbox.routing.registerRoute(
    /.*(?:cloudflare)\.com/,
    new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'googleapis',
})
);

workbox.routing.registerRoute(
    /.*(?:googleapis)\.com/,
    new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'googleapis',
})
);

workbox.routing.registerRoute(
    /.*(?:gstatic)\.com/,
    new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'gstatic',
})
);

const FALLBACK_HTML_URL = '/Offline/';
const FALLBACK_IMAGE_URL = '/Content/images/offline150.png';
const FALLBACK_FONT_URL = '/Offline/';

workbox.routing.setCatchHandler(({ url, event, params }) => {
    //console.log("event: ", event);
    switch (event.request.destination) {
        case 'document':
            return caches.match(FALLBACK_HTML_URL);
        case 'image':
            return caches.match(FALLBACK_IMAGE_URL);
            break;
        case 'font':
            return caches.match(FALLBACK_FONT_URL);
            break;
        default:
            // If we don't have a fallback, just return an error response.
            return Response.error();
}
});

const CACHE_FILES = [
    '/',
    '/Offline/',
    '/Home/',
];

const precacheController = new workbox.precaching.PrecacheController();
precacheController.addToCacheList(CACHE_FILES);

workbox.precaching.precacheAndRoute([], {
    // Ignore all URL parameters.
    ignoreURLParametersMatching: [/^[^.]+$|\.(?!(orig)$)([^.]+$)/],
    cleanUrls: false,
    directoryIndex: null,
});

workbox.precaching.cleanupOutdatedCaches();