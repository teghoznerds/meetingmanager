var path = require('path');
global.appRoot = path.resolve(__dirname);

module.exports = {
    globDirectory: appRoot,
    //globDirectory: "/content",
    globPatterns: [
        "**/*.{png,jpg,gif,css,eot,ttf,woff,woff2,otf,PNG,js,ts,json,txt,html,ico,GIF,csv}"//svg,xap,swf,xml
    ],
    globIgnores: [
        "**/node_modules/**/*",
        "**/Uploads/**/*",   
        "**/sw.js",
         "**/workbox-config.js",
        "**/src-sw.js",
        "/^[^.]+$|\.(?!(orig)$)([^.]+$)/"
    ],
  swDest: "sw.js",
  swSrc: "src-sw.js",
  globStrict: false,
};