var express = require('express');
const app = express();
var serveStatic = require('serve-static');
var path = require("path");
var formidable = require('formidable');
var finalhandler = require('finalhandler');
var fs = require('fs');
var serve = serveStatic(__dirname);
var livereload = require('livereload');
var os = require('os');
var csvalidator = require('./csv-validator');
const csv = require('csvtojson');
var opener = require("opener");

class AfrinicRouter{
    constructor(port, host){
        this._port = port;
        this._host = host;
    }
    Handlers(){
        app.use('/', express.static(__dirname));

        app.get('/GetUsername', function (req, res) {
            var username = os.userInfo().username;
            res.send(username);
        })

        app.get('/Admin', function (req, res) {
            let url = `${__dirname}/Uploads/`;
            fs.exists(url, (exist) =>{
                if(exist == false){
                    fs.mkdir(url, (done) => {
                        
                    });
                }
            }); 
            res.sendFile(path.join(__dirname + '/Admin.html'));
        });

        app.get('/Timeline', function (req, res) {
            res.sendFile(path.join(__dirname + '/Timeline.html'));
        });

        app.get('/List', function (req, res) {
            res.sendFile(path.join(__dirname + '/List.html'));
        });

        app.get('/Offline', function (req, res) {
            res.sendFile(path.join(__dirname + '/offline.html'));
        });

        app.get('/Tests', function (req, res) {
            res.sendFile(path.join(__dirname + '/Test.html'));
        });

        app.get('/FormatRules', function (req, res) {
            res.sendFile(path.join(__dirname + '/format-rules.html'));
        });


        app.get('/CheckIfCSVExist', function (req, res) {
            fs.exists(`${__dirname}/Uploads/CSVFILE.csv`, (exist) =>{
                res.send(exist);
            });   
        });

        app.get('/GetCSVFileDetails', function (req, res) {
            var fpath = `${__dirname}/Uploads/CSVFILE.csv`;
            fs.stat(fpath, function(err, stats) {
                stats.fileName = path.basename(fpath, path.extname(fpath))
                res.send(stats);
            });
        });

        app.get('/DeleteCSVFile', function (req, res) {
            var fpath = `${__dirname}/Uploads/CSVFILE.csv`;
            fs.unlink(fpath, (err) => {
            });

            res.send(true);
        });

        app.get('/fileupload', function (req, res) {
            var html = `<html>
                        <body>
                            <form method="post" action="http://localhost:3000">Name: 
                                <input type="text" name="name" />
                                <input type="submit" value="Submit" />
                                    </form>
                                </body>
                            </html>`
                            res.writeHead(200, {'Content-Type': 'text/html'})
                            res.end(html)
        });

        app.post('/fileupload', function (req, res) {
            try{
                var form = new formidable.IncomingForm();
                form.parse(req, function (err, fields, files) {

                    if(files != null){
                        var oldpath = files.filetoupload.path;

                        var validator = new csvalidator(oldpath, csv);
                        var regex = RegExp(/^[a-zA-Z ]*$/);
                        validator.isValid(regex, "Title", (result) => {
                            if(result.status !== undefined){
                                if(result.status){
                                    var newpath = __dirname +  '/Uploads/CSVFILE.csv';
    
                                    fs.copyFile(oldpath, newpath, (err) => {
                                        if (err) throw err;
                                        console.log('source.txt was copied to destination.txt');                                                                 
                                    });                                                                          
                                }

                                res.redirect(`/Admin?msg=${result.message}&status=${result.status ? "success" : "danger"}`);
                            }                           

                            fs.unlink(oldpath, (err) => {
                            });
                        });                                      
                    }               
                });
            }
            catch(ex){
                res.redirect(`/Admin?msg=${ex}`);
            }           
        });

        app.get('/CSV', function(req, res){  
            var fpath = `${__dirname}/Uploads/CSVFILE.csv`;

            if(fs.existsSync(fpath)){
                csv().fromFile(fpath).then((jsonObj)=>{
                    res.send(jsonObj);
                });
            } 
            else{
                res.send("File does not exist");
            }           
        });
        
        const port = this._port;
        const host = this._host;
        
        
        var lrserver = livereload.createServer();
        lrserver.watch(__dirname + "/public");
        app.listen(port, () => console.log(`Afrinic meeting app listening on port ${port}!`));
        opener(`http://${host}:${port}`);
    }
}

module.exports = AfrinicRouter;