# MeetingManager
Meeting Mananger is a demo for recruitment purpose. See more details about the [task](about-meeting-manager.md) here.

![picture alt](images/meeting-manager.png "Meeting Manager")

## Requirement
**Node JS.**

### Check NodeJS Existence
```bash
node -v
```
Run the above command on terminal. If no version number is outputted, you probably dont have node installed.

### NodeJS Installation
Please see - https://nodejs.org/en/download/

## Usage
Use [NPM](https://www.npmjs.com/get-npm) to run the demo. It might be a good idea to give admin rights to the project folder

### Install dependencies
```bash
npm install
```
### Run Mocha Test
```bash
npm test
```
### Run Demo
```bash
npm run demo
```
### Frontend Test
See [Qunit Test](js/afrinic-test.js).

### NodeJS Test
See [Mocha Test](test/server-test.js).

### Sample Code
See [List View](js/afrinic-list.js).

# Features
* HTML5
* Javascript
* NodeJS
* QUnit
* Mocha
* Workbox (PWA)
![picture alt](images/install-meeting.png "Install Meeting Manager")