﻿importScripts('https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js');

if (workbox) {
    console.log(`Yay! Workbox is loaded 🎉`);
} else {
    console.log(`Boo! Workbox didn't load 😬`);
}

//Force Developent/Production Builds
workbox.setConfig({ debug: true });

//Initialize google analytics
workbox.googleAnalytics.initialize();

// Enable navigation preload.
workbox.navigationPreload.enable();

//take control immediately
workbox.core.skipWaiting();
workbox.core.clientsClaim();

const CACHE_VERSION = "1.0.0.4";
const CACHE_NAME = "Afrinic-" + "-" + CACHE_VERSION;

//This is our custom handler to recieve pushed notifications
self.addEventListener('push', event => {
    //console.log("event: ", event);
    const options = {
        body: event.data.text(),
        icon: params.notificationicon,
        vibrate: [100, 50, 100],
        data: {
            dateOfArrival: Date.now(),
            primaryKey: 1
        },
        actions: [
            {
                action: '/', title: 'Go to '+params.siteshortname,
                icon: '/content/yes.png'
            },
            {
                action: 'close', title: 'Close the notification',
                icon: '/content/no.png'
            },
        ]
    };

    event.waitUntil(
        self.registration.showNotification('Push Notification', options)
    );
});

const propertyHomeHandler = new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'afrinic-pages',
    plugins: [
      new workbox.expiration.Plugin({
          maxEntries: 50,
      })
    ]
});

const offlineHandler = new workbox.strategies.CacheFirst({
    cacheName: 'offline-page',
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 50,
        })
    ]
});

workbox.core.setCacheNameDetails({
	prefix: CACHE_NAME + '',
	suffix: CACHE_NAME + '',
	precache: CACHE_NAME + '-precache',
	runtime: CACHE_NAME+ '-runtime'
});

workbox.routing.registerRoute('/', args => {
    return propertyHomeHandler.handle(args).then(response => {
        if (!response) {
            return caches.match('/Offline/');
        } else if (response.status === 404) {
            return caches.match('/Offline/');
        }
        return response;
    });
});

workbox.routing.registerRoute('/Home/Index/', args => {
    return propertyHomeHandler.handle(args).then(response => {
        if (!response) {
            return caches.match('/Offline/');
} else if (response.status === 404) {
            return caches.match('/Offline/');
}
        return response;
});
});

workbox.routing.registerRoute('/Offline/', args => {
    return offlineHandler.handle(args).then(response => {
        if (!response) {
            return caches.match('/Offline.html');
} else if (response.status === 404) {
            return caches.match('/Offline.html');
}
        return response;
});
});

workbox.routing.registerRoute('/fonts/*',
    new workbox.strategies.CacheOnly()
);

workbox.routing.registerRoute('/vendor/*',
    new workbox.strategies.CacheOnly()
);


workbox.routing.registerRoute('/css/*',
    new workbox.strategies.CacheOnly()
);

workbox.routing.registerRoute('/js/*',
    new workbox.strategies.CacheOnly()
);

//handle all CDN related request
workbox.routing.registerRoute(
    /\.(?:js|css)$/,
    new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'static-resources',
})
);

workbox.routing.registerRoute(
  '/\.(?:png|gif|jpg|jpeg|svg)$/',
  new workbox.strategies.CacheFirst({
    cacheName: 'images',
    plugins: [
        new workbox.expiration.Plugin({
    maxEntries: 60,
    maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
}),
],
}),
);

workbox.routing.registerRoute(
    /.*(?:bootstrapcdn)\.com/,
    new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'googleapis',
})
);


workbox.routing.registerRoute(
    /.*(?:aspnetcdn)\.com/,
    new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'googleapis',
})
);

workbox.routing.registerRoute(
    /.*(?:cloudflare)\.com/,
    new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'googleapis',
})
);

workbox.routing.registerRoute(
    /.*(?:googleapis)\.com/,
    new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'googleapis',
})
);

workbox.routing.registerRoute(
    /.*(?:gstatic)\.com/,
    new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'gstatic',
})
);

const FALLBACK_HTML_URL = '/Offline/';
const FALLBACK_IMAGE_URL = '/Content/images/offline150.png';
const FALLBACK_FONT_URL = '/Offline/';

workbox.routing.setCatchHandler(({ url, event, params }) => {
    //console.log("event: ", event);
    switch (event.request.destination) {
        case 'document':
            return caches.match(FALLBACK_HTML_URL);
        case 'image':
            return caches.match(FALLBACK_IMAGE_URL);
            break;
        case 'font':
            return caches.match(FALLBACK_FONT_URL);
            break;
        default:
            // If we don't have a fallback, just return an error response.
            return Response.error();
}
});

const CACHE_FILES = [
    '/',
    '/Offline/',
    '/Home/',
];

const precacheController = new workbox.precaching.PrecacheController();
precacheController.addToCacheList(CACHE_FILES);

workbox.precaching.precacheAndRoute([
  {
    "url": "admin.html",
    "revision": "b12086a58fe2a3c2feb920ce1d384f1e"
  },
  {
    "url": "css/afrinic.css",
    "revision": "c95d4dd5ccbf69f462c6109a5f5e1f4a"
  },
  {
    "url": "css/font-face.css",
    "revision": "834041972ae64e6250fb9566a2c9cd97"
  },
  {
    "url": "css/style-albe-timeline.css",
    "revision": "4bc3cef2635cb2498675eca3097de5d7"
  },
  {
    "url": "css/theme.css",
    "revision": "2dc8b8ba6a2fd61da864104965c1c065"
  },
  {
    "url": "csv-validator.js",
    "revision": "cfc3adfaad662ef289ec03f881f68404"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-100.eot",
    "revision": "8da0d715feeec47c81b3d02343fafdd3"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-100.ttf",
    "revision": "8732c754f71735b9f945f7c715fb5990"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-100.woff",
    "revision": "7f5b902dea02c28dc1183d7f623aa0fb"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-100.woff2",
    "revision": "f4ed853682a1ca4d3c5147265872a538"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-100italic.eot",
    "revision": "9b217e995226ac4e68179a2b6b515262"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-100italic.ttf",
    "revision": "abe17efb66de7738e0d244ee967fe839"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-100italic.woff",
    "revision": "3ed2510a3f1ba88e0dfc61cb1966b4b4"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-100italic.woff2",
    "revision": "ee845b5b261939c73e21ae3bf299aaf8"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-200.eot",
    "revision": "d6f66516d2b664413479c62e42adb25e"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-200.ttf",
    "revision": "7b8aa833969719798af3b5999c7437f3"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-200.woff",
    "revision": "d7ef15c0b436fac613b1900fab84c93f"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-200.woff2",
    "revision": "fba994d4045c23721ed05504854a5357"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-200italic.eot",
    "revision": "e67259c315859109731cce7d7f2bb19c"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-200italic.ttf",
    "revision": "95938803931aa5081741c8ef2d81cd71"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-200italic.woff",
    "revision": "9626a4b716b0fabc582171a7db38b0b5"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-200italic.woff2",
    "revision": "0fb363df3d0a40b613b9332d44b1d2c2"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-300.eot",
    "revision": "2e0847ba0b1e237882854e8dab9da109"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-300.ttf",
    "revision": "65bbf50d80ffbd75536abea87a4962db"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-300.woff",
    "revision": "dad8b32d6402d45efbd9f2a8ee6f203a"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-300.woff2",
    "revision": "f6ef809fb833dbf1abcd5b35ab1576e5"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-300italic.eot",
    "revision": "81f9f9329c89f478b8f3cf8bc58f4c5c"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-300italic.ttf",
    "revision": "00b670ebdb4b1c61f8810fae255cb963"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-300italic.woff",
    "revision": "ffcf1f1c66a6661dabcf9a52581186d5"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-300italic.woff2",
    "revision": "5e4a0e72a17dffda2e8c2af3af358110"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-500.eot",
    "revision": "12a5a771441907d9570020c1717158c3"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-500.ttf",
    "revision": "22b7688b2941298f2afd730763ef61d9"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-500.woff",
    "revision": "0261e08bd22d9f91c1d277cd4874ec95"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-500.woff2",
    "revision": "08609a017d830988630ee1b38a7ef71a"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-500italic.eot",
    "revision": "7aca43b708f8dde583fbaa9bbbf8e1f5"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-500italic.ttf",
    "revision": "9972bab7961b40e7da377f55c418e215"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-500italic.woff",
    "revision": "fabb1cda13707046efa0f7238555cc01"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-500italic.woff2",
    "revision": "a43a4a36fcfe4d073146b54490eec662"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-600.eot",
    "revision": "693057428838523377f5c2da26f44899"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-600.ttf",
    "revision": "b01250b38f0ef1dc59107894d37711ec"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-600.woff",
    "revision": "02faed9f2938bbba57514dfbea590d56"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-600.woff2",
    "revision": "d01ac68c4747e41a359cd94cecc29f9f"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-600italic.eot",
    "revision": "ad3b67d74a9d06a2b3c33e303f709576"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-600italic.ttf",
    "revision": "811ca08800ce75bf36b23bddbba7c5b0"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-600italic.woff",
    "revision": "4624d18c5dd4c89fa2055339db8641ce"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-600italic.woff2",
    "revision": "fa071664f519e9e9409d8a6614b7c854"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-700.eot",
    "revision": "319537c2e34754b8482dc10b5a5a678f"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-700.ttf",
    "revision": "44bb1bc773b816dfb13a88f43d0d0476"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-700.woff",
    "revision": "d1d7b85dc76bad1a967fc842d110262c"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-700.woff2",
    "revision": "d9f0e4b31953d6440559059230442358"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-700italic.eot",
    "revision": "ef75019674fb2adad506e031bd14331e"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-700italic.ttf",
    "revision": "0470693a3857401dad7c0ca10db65617"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-700italic.woff",
    "revision": "d03f7611353f6f9e728b329d982b7660"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-700italic.woff2",
    "revision": "da80a603d0220f8e5bb3aaf0e5b42763"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-800.eot",
    "revision": "2f8f4d43aac53d1b881fcd50b08ac5e8"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-800.ttf",
    "revision": "97ae8f0f4c3d743a27496689665416bc"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-800.woff",
    "revision": "99a9e7bac31ddfc2b58e0025f76c65d1"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-800.woff2",
    "revision": "6f22cd6faf712f6973ee0f38cd38c0e6"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-800italic.eot",
    "revision": "cc33beefca3fdaae55ac852f32278166"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-800italic.ttf",
    "revision": "e85ba4fc01dc62bf49b51b337b710ca8"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-800italic.woff",
    "revision": "5c8a310a6164fe0dfec074a89d94f181"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-800italic.woff2",
    "revision": "acbf8d91de92514c5111f02cb26854a9"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-900.eot",
    "revision": "480a646050fb8b991ebff5543ee756b0"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-900.ttf",
    "revision": "11c0d35b23006eefd198bb52e3e617f9"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-900.woff",
    "revision": "1b8eddbe62c11ea9eb4f407ecd897827"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-900.woff2",
    "revision": "f53d38d7a9ce9fb32f278410609cd7d4"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-900italic.eot",
    "revision": "db8e4e4c819b0f47d52206c9ce93bf99"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-900italic.ttf",
    "revision": "f42e96dd6f4e67ab15d887847e2f706f"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-900italic.woff",
    "revision": "1250c5ebf29c488f329a5ade53a936e5"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-900italic.woff2",
    "revision": "470d942f71759896d116a87d770a8c2e"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-italic.eot",
    "revision": "9741803691bc57d55082d53237380709"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-italic.ttf",
    "revision": "506db506cbae0183e1b2c50d069d3769"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-italic.woff",
    "revision": "055f0699f59e8b1eb28270b304c5d11b"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-italic.woff2",
    "revision": "936aaefc2efb764f3f52c0e9f34ef749"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-regular.eot",
    "revision": "f00ab8be77768ee0afe76d8fa105e7ee"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-regular.ttf",
    "revision": "4711bbc2872e5e734fe5d84804f24967"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-regular.woff",
    "revision": "1fce830e6112511a77108832e13172fd"
  },
  {
    "url": "fonts/poppins/poppins-v5-latin-regular.woff2",
    "revision": "ce0c9ae08840a0b43bccb9f5a86e155d"
  },
  {
    "url": "format-rules.html",
    "revision": "0c30aa7f1a8321e0b95172527abf243d"
  },
  {
    "url": "images/AAPSI-Ao.png",
    "revision": "35b949e552f9e028b637fc4336670ef0"
  },
  {
    "url": "images/eventone.png",
    "revision": "81d4a1dbb24d372c527f7cc3463b69ee"
  },
  {
    "url": "images/GovAo.png",
    "revision": "d2eeef8fdfc28955569286e3ee72e52b"
  },
  {
    "url": "images/icon/Afrinic144.png",
    "revision": "3aaa01d0e6cc2d88957ebe35532b125f"
  },
  {
    "url": "images/icon/Afrinic192.png",
    "revision": "63160844c9af47fa47faa1248cbbc77b"
  },
  {
    "url": "images/icon/Afrinic512.png",
    "revision": "8b440aead436f0203e21c60ebd6d20bb"
  },
  {
    "url": "images/icon/afrnic.png",
    "revision": "e3ff126f6c31f21866bc18bff186d552"
  },
  {
    "url": "images/icon/avatar-01.jpg",
    "revision": "2b183337c146ae1acea2c6204ab9d419"
  },
  {
    "url": "images/icon/logo-blue.png",
    "revision": "9bb1767b6ecc9f781b9a69332b5d5253"
  },
  {
    "url": "images/icon/logo-mini.png",
    "revision": "bbf3334161b7bbd135a0bbd79178817a"
  },
  {
    "url": "images/icon/logo-white.png",
    "revision": "7962e4dafacd8f6b254542e43f02c6e4"
  },
  {
    "url": "images/icon/logo.png",
    "revision": "3aab711ec1d99df58613c0168da9ba97"
  },
  {
    "url": "images/install-meeting.png",
    "revision": "464a04207f90c71a531b01789419ae09"
  },
  {
    "url": "images/lunchtime.png",
    "revision": "3ba9fc1ec719a01e769a9721c63c61ef"
  },
  {
    "url": "images/meeting-manager.png",
    "revision": "5cac32d8dc90bf6574592072e12747fd"
  },
  {
    "url": "images/Offline.png",
    "revision": "df99b734737949c6ade02a11a7b261e1"
  },
  {
    "url": "index.html",
    "revision": "8e4302c6468a2f47c63acac39089e9ee"
  },
  {
    "url": "js/afrinic-Base.js",
    "revision": "b490a7c85d28e5248d1948699f90d7b6"
  },
  {
    "url": "js/afrinic-front.js",
    "revision": "0721df1e82cb472ff80cb71c9ccdff09"
  },
  {
    "url": "js/afrinic-list.js",
    "revision": "5b94694fd8df54687a41feedbd9c3c9a"
  },
  {
    "url": "js/afrinic-test.js",
    "revision": "db95d1c108457566fc1f4dfc539e58a4"
  },
  {
    "url": "js/afrinic-timeline.js",
    "revision": "c1122c6f80328c0470ab349d7d68264f"
  },
  {
    "url": "js/afrinic-UI.js",
    "revision": "b05d3652648b5d0ebec779d10f558b86"
  },
  {
    "url": "js/afrinic-Utils.js",
    "revision": "8daf93104a33e866c4eb3c118f128e6b"
  },
  {
    "url": "js/afrinic.js",
    "revision": "2892e3b75b39eaabf4c8fd1ddb3f4524"
  },
  {
    "url": "js/data-albe-timeline.js",
    "revision": "b2370dfc00826d73568cdfbacfe36bce"
  },
  {
    "url": "js/main.js",
    "revision": "1ed989b32f4d2e435f429651d474b536"
  },
  {
    "url": "List.html",
    "revision": "d82dac2eb1cfc6f45f10c2ba84adfa02"
  },
  {
    "url": "manifest.json",
    "revision": "1c9d85c1e5aec1671f619d9c9b556c6b"
  },
  {
    "url": "offline.html",
    "revision": "0038093e1b921f33f493ca4e8783190f"
  },
  {
    "url": "package-lock.json",
    "revision": "5afdc96f34f20074bfe07c0736d798af"
  },
  {opener("http://google.com");
    "url": "package.json",
    "revision": "2c9fe372d9118c85e286b5e9e1e124fd"
  },
  {
    "url": "router.js",
    "revision": "d5d77aba7fa2f268ffbe2ff09d589897"
  },
  {
    "url": "sample_data/CSVBADFILE.csv",
    "revision": "7079df036f1c7c0591c54202d36682a2"
  },
  {
    "url": "sample_data/CSVGOODFILE.csv",
    "revision": "3131ec3ab01a2096e49d3de84bf86b70"
  },
  {
    "url": "server.js",
    "revision": "1f358cefe173e6fcf3572e6ed103ce21"
  },
  {
    "url": "templates/alert.html",
    "revision": "0dc1c1883d447b437a849546b5ff3338"
  },
  {
    "url": "templates/day-template.html",
    "revision": "baf8f76714aa7040cb8ba20384bb3f02"
  },
  {
    "url": "templates/event-item-template.html",
    "revision": "71dccb98218bf9bcfd01a7315ae31ecb"
  },
  {
    "url": "templates/event-template.html",
    "revision": "1175563b405e2b30746b6c1cce5fd014"
  },
  {
    "url": "templates/session-template.html",
    "revision": "fb0acd7091a7b774e697d347082181eb"
  },
  {
    "url": "Test.html",
    "revision": "27a7b24097fef030f9b26020555d6b13"
  },
  {
    "url": "test/server-test.js",
    "revision": "baed4b442a122128c2e7a971907e44ed"
  },
  {
    "url": "test/test-csv.csv",
    "revision": "99239aa0e5d7bdd722b28f68dcc9e297"
  },
  {
    "url": "Timeline.html",
    "revision": "6b463d9a3b3a41d8e79e569a33dff060"
  },
  {
    "url": "vendor/animsition/animsition.min.css",
    "revision": "a92fda81eb047840737ff97ef8a9ed95"
  },
  {
    "url": "vendor/animsition/animsition.min.js",
    "revision": "a0ddf9fddbe7c04867860fd24eb6c652"
  },
  {
    "url": "vendor/bootstrap-4.1/bootstrap.min.css",
    "revision": "9085ab0d9dc4f08b981ba6b6766fd2bb"
  },
  {
    "url": "vendor/bootstrap-4.1/bootstrap.min.js",
    "revision": "95697eefe013ce1a1e69c14105d09696"
  },
  {
    "url": "vendor/bootstrap-4.1/popper.min.js",
    "revision": "6e1e51ca997cad6c8367d797b3f2946c"
  },
  {
    "url": "vendor/css-hamburgers/hamburgers.css",
    "revision": "dd2d89b7d71cd354d0a4aac86a5c253c"
  },
  {
    "url": "vendor/css-hamburgers/hamburgers.min.css",
    "revision": "23286ce959853a53dbb07f137e56eb8f"
  },
  {
    "url": "vendor/font-awesome-4.7/css/font-awesome.css",
    "revision": "4bb3dd721c4652feee0953261d329710"
  },
  {
    "url": "vendor/font-awesome-4.7/css/font-awesome.min.css",
    "revision": "a0e784c4ca94c271b0338dfb02055be6"
  },
  {
    "url": "vendor/font-awesome-4.7/fonts/fontawesome-webfont.eot",
    "revision": "674f50d287a8c48dc19ba404d20fe713"
  },
  {
    "url": "vendor/font-awesome-4.7/fonts/fontawesome-webfont.ttf",
    "revision": "b06871f281fee6b241d60582ae9369b9"
  },
  {
    "url": "vendor/font-awesome-4.7/fonts/fontawesome-webfont.woff",
    "revision": "fee66e712a8a08eef5805a46892932ad"
  },
  {
    "url": "vendor/font-awesome-4.7/fonts/fontawesome-webfont.woff2",
    "revision": "af7ae505a9eed503f8b8e6982036873e"
  },
  {
    "url": "vendor/font-awesome-4.7/fonts/FontAwesome.otf",
    "revision": "0d2717cd5d853e5c765ca032dfd41a4d"
  },
  {
    "url": "vendor/font-awesome-4.7/HELP-US-OUT.txt",
    "revision": "e5f4d96ed70c0c372ccf7a0d9841ba53"
  },
  {
    "url": "vendor/font-awesome-5/css/fontawesome-all.min.css",
    "revision": "900c764bb61e8efc98f2a8b96acf6781"
  },
  {
    "url": "vendor/font-awesome-5/webfonts/fa-brands-400.eot",
    "revision": "13db00b7a34fee4d819ab7f9838cc428"
  },
  {
    "url": "vendor/font-awesome-5/webfonts/fa-brands-400.ttf",
    "revision": "c5ebe0b32dc1b5cc449a76c4204d13bb"
  },
  {
    "url": "vendor/font-awesome-5/webfonts/fa-brands-400.woff",
    "revision": "a046592bac8f2fd96e994733faf3858c"
  },
  {
    "url": "vendor/font-awesome-5/webfonts/fa-brands-400.woff2",
    "revision": "e8c322de9658cbeb8a774b6624167c2c"
  },
  {
    "url": "vendor/font-awesome-5/webfonts/fa-regular-400.eot",
    "revision": "701ae6abd4719e9c2ada3535a497b341"
  },
  {
    "url": "vendor/font-awesome-5/webfonts/fa-regular-400.ttf",
    "revision": "ad97afd3337e8cda302d10ff5a4026b8"
  },
  {
    "url": "vendor/font-awesome-5/webfonts/fa-regular-400.woff",
    "revision": "ef60a4f6c25ef7f39f2d25a748dbecfe"
  },
  {
    "url": "vendor/font-awesome-5/webfonts/fa-regular-400.woff2",
    "revision": "cd6c777f1945164224dee082abaea03a"
  },
  {
    "url": "vendor/font-awesome-5/webfonts/fa-solid-900.eot",
    "revision": "8e3c7f5520f5ae906c6cf6d7f3ddcd19"
  },
  {
    "url": "vendor/font-awesome-5/webfonts/fa-solid-900.ttf",
    "revision": "b87b9ba532ace76ae9f6edfe9f72ded2"
  },
  {
    "url": "vendor/font-awesome-5/webfonts/fa-solid-900.woff",
    "revision": "faff92145777a3cbaf8e7367b4807987"
  },
  {
    "url": "vendor/font-awesome-5/webfonts/fa-solid-900.woff2",
    "revision": "0ab54153eeeca0ce03978cc463b257f7"
  },
  {
    "url": "vendor/jquery-3.2.1.min.js",
    "revision": "473957cfb255a781b42cb2af51d54a3b"
  },
  {
    "url": "vendor/jquery-albe-timeline.js",
    "revision": "2a45deadb67bb1be9b69bfe869f28ee0"
  },
  {
    "url": "vendor/jquery-ui.min.js",
    "revision": "c04145e6dcd756ccce2bfe588b5d5bd6"
  },
  {
    "url": "vendor/mdi-font/css/material-design-iconic-font.css",
    "revision": "612a746cc755cfd3ceace05a85ab0da5"
  },
  {
    "url": "vendor/mdi-font/css/material-design-iconic-font.min.css",
    "revision": "e9365fe85b7e4db79a87015e52c3db6c"
  },
  {
    "url": "vendor/mdi-font/fonts/Material-Design-Iconic-Font.eot",
    "revision": "e833b2e2471274c238c0553f11031e6a"
  },
  {
    "url": "vendor/mdi-font/fonts/Material-Design-Iconic-Font.ttf",
    "revision": "b351bd62abcd96e924d9f44a3da169a7"
  },
  {
    "url": "vendor/mdi-font/fonts/Material-Design-Iconic-Font.woff",
    "revision": "d2a55d331bdd1a7ea97a8a1fbb3c569c"
  },
  {
    "url": "vendor/mdi-font/fonts/Material-Design-Iconic-Font.woff2",
    "revision": "a4d31128b633bc0b1cc1f18a34fb3851"
  },
  {
    "url": "vendor/perfect-scrollbar/perfect-scrollbar.css",
    "revision": "0e66ce099a973840fe7f6c5bc63be390"
  },
  {
    "url": "vendor/perfect-scrollbar/perfect-scrollbar.js",
    "revision": "0f5ec0fddd5eb2ba440bc8b057a2856a"
  },
  {
    "url": "vendor/perfect-scrollbar/perfect-scrollbar.min.js",
    "revision": "8b5109d965296ad5147e7cd372f9b472"
  },
  {
    "url": "vendor/progressbar/progressbar.js",
    "revision": "3eaff5ea39c9b19acb24853185d2c844"
  },
  {
    "url": "vendor/progressbar/progressbar.min.js",
    "revision": "a2ad947bd096bfc0a832273e40386877"
  },
  {
    "url": "vendor/sweetalert/sweetalert.min.js",
    "revision": "03d846cf1a557194cd1d386455d93c1a"
  },
  {
    "url": "vendor/wow/animate.css",
    "revision": "16a90b684e3369da090ecdb6bb32107c"
  },
  {
    "url": "vendor/wow/wow.min.js",
    "revision": "e1f1ff6897992a9165e8ce009b4039e3"
  }
], {
    // Ignore all URL parameters.
    ignoreURLParametersMatching: [/^[^.]+$|\.(?!(orig)$)([^.]+$)/],
    cleanUrls: false,
    directoryIndex: null,
});

workbox.precaching.cleanupOutdatedCaches();